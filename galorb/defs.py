try:
    import astropy.coordinates as coords
    LG_bsr = coords.LSR()
    v_bary = LG_bsr.v_bary.d_xyz.value  # km/s
    Sol_gsr = coords.Galactocentric()
    gal_cen_distance = Sol_gsr.galcen_distance.value  # kpc
    gal_rot_velocity = Sol_gsr.galcen_v_sun.d_y.value - v_bary[1]  # km/s
except ImportError:
    gal_cen_distance = 8.  # kpc
    gal_rot_velocity = 220.  # km/s
    v_bary = [10.3, 15.3, 7.7]  # km/s


target_def = {
    'ra': None,
    'dec': None,
    'glat': None,
    'glon': None,
    'distance': None,  # pc
    'pm': None,  # proper motion
    'vr': None}  # radial velocity (heliocentric)

# -fin-
