from __future__ import print_function
import matplotlib.pylab as plt


# Display orbits
def xyz_plot(ts, orbit,
             r0=8.,
             v0=220.,
             clr='k',
             ls='-',
             axis=None,
             label=None,
             singles=False):
    from astropy import units as u
    from galpy.orbit import Orbit
    import galpy

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
    else:
        ax = axis[0]

    Sol_orbit = Orbit(vxvv=[r0 * u.kpc,
                            22. * u.km / u.s,
                            v0 * u.km / u.s,
                            0. * u.kpc,
                            22. * u.km / u.s,
                            0. * u.deg])
    Sol_orbit.flip(inplace=True)
    Sol_orbit.integrate(ts, galpy.potential.MWPotential2014, method='rk4_c')
    ax.plot(Sol_orbit.y(ts), Sol_orbit.x(ts), color='k', alpha=0.3, linestyle=':')
    ax.plot(Sol_orbit.y(ts)[0], Sol_orbit.x(ts)[0], 'kv', alpha=0.5)
    ax.plot(Sol_orbit.y(ts)[-1], Sol_orbit.x(ts)[-1], 'ko', alpha=0.5)

    ax.plot(orbit.y(ts),
            orbit.x(ts),
            color=clr,
            linestyle=ls)
    ax.plot(orbit.y(ts)[0],
            orbit.x(ts)[0],
            color=clr,
            marker='v',
            linestyle=ls,
            label='%s now' % label)
    ax.plot(orbit.y(ts)[-1],
            orbit.x(ts)[-1],
            color=clr,
            marker='o',
            linestyle=ls,
            label='%s 1Gyr ago' % label)
    ax.set_xlim([-10, 10])
    ax.set_xlabel(r'$\mathbf{y\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')
    ax.set_ylabel(r'$\mathbf{x\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
    else:
        ax = axis[1]
    ax.plot(orbit.x(ts),
            orbit.z(ts),
            color=clr,
            linestyle=ls)
    ax.plot(orbit.x(ts)[0],
            orbit.z(ts)[0],
            color=clr,
            marker='v',
            label='%s now' % label)
    ax.plot(orbit.x(ts)[-1],
            orbit.z(ts)[-1],
            color=clr,
            marker='o',
            label='%s 1Gyr ago' % label)
    ax.set_xlim([-10, 10])
    ax.invert_xaxis()
    ax.set_xlabel(r'$\mathbf{x\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')
    ax.set_ylabel(r'$\mathbf{z\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
    else:
        ax = axis[2]
    ax.plot(orbit.y(ts),
            orbit.z(ts),
            color=clr,
            linestyle=ls)
    ax.plot(orbit.y(ts)[0],
            orbit.z(ts)[0],
            color=clr,
            marker='v',
            label='%s now' % label)
    ax.plot(orbit.y(ts)[-1],
            orbit.z(ts)[-1],
            color=clr,
            marker='o',
            label='%s 1Gyr ago' % label)
    ax.set_xlim([-10, 10])
    ax.set_xlabel(r'$\mathbf{y\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')
    ax.set_ylabel(r'$\mathbf{z\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')


# Plot overlap of all orbits radial components vs time
def radius(ts, orbit,
           clr='k',
           ls='-',
           axis=None,
           singles=False):

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
    else:
        ax = axis[0]
    ax.plot(ts, orbit.R(ts), color=clr, linestyle=ls)
    ax.invert_xaxis()
    ax.set_ylabel(r'$\mathbf{R\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')
    ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                  fontsize=14,
                  fontweight='bold')

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
    else:
        ax = axis[1]
    ax.plot(ts, orbit.vR(ts), color=clr, linestyle=ls)
    ax.invert_xaxis()
    ax.set_ylabel(r'$\mathbf{v_R\ (km/s)}$',
                  fontsize=14,
                  fontweight='bold')
    ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                  fontsize=14,
                  fontweight='bold')


# Plot overlap of all orbits x,y,z vs time
def timeplot(ts, orbit,
             clr='k',
             ls='-',
             axis=None,
             singles=False):

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
        ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                      fontsize=14,
                      fontweight='bold')
    else:
        ax = axis[0, 0]
    ax.plot(ts, orbit.x(ts), color=clr, linestyle=ls)
    ax.invert_xaxis()
    ax.set_ylabel(r'$\mathbf{x\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
        ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                      fontsize=14,
                      fontweight='bold')
    else:
        ax = axis[1, 0]
    ax.plot(ts, orbit.y(ts), color=clr, linestyle=ls)
    ax.invert_xaxis()
    ax.set_ylabel(r'$\mathbf{y\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
        ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                      fontsize=14,
                      fontweight='bold')
    else:
        ax = axis[2, 0]
        ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                      fontsize=14,
                      fontweight='bold')
    ax.plot(ts, orbit.z(ts), color=clr, linestyle=ls)
    ax.invert_xaxis()
    ax.set_ylabel(r'$\mathbf{z\ (kpc)}$',
                  fontsize=14,
                  fontweight='bold')

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
        ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                      fontsize=14,
                      fontweight='bold')
    else:
        ax = axis[0, 1]
    ax.plot(ts, orbit.vx(ts), color=clr, linestyle=ls)
    ax.invert_xaxis()
    ax.set_ylabel(r'$\mathbf{v_x\ (km/s)}$',
                  fontsize=14,
                  fontweight='bold')

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
        ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                      fontsize=14,
                      fontweight='bold')
    else:
        ax = axis[1, 1]
    ax.plot(ts, orbit.vy(ts), color=clr, linestyle=ls)
    ax.invert_xaxis()
    ax.set_ylabel(r'$\mathbf{v_y\ (km/s)}$',
                  fontsize=14,
                  fontweight='bold')

    if singles:
        fig, axis = plt.subplots(figsize=(7, 5),
                                 facecolor='white')
        ax = axis
        ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                      fontsize=14,
                      fontweight='bold')
    else:
        ax = axis[2, 1]
        ax.set_xlabel(r'$\mathbf{t\ (Gyr)}$',
                      fontsize=14,
                      fontweight='bold')
    ax.plot(ts, orbit.vz(ts), color=clr, linestyle=ls)
    ax.invert_xaxis()
    ax.set_ylabel(r'$\mathbf{v_z\ (km/s)}$',
                  fontsize=14,
                  fontweight='bold')

# -fin-
