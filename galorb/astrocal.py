# Custom library for converting heliocentric values to LSR

import numpy as np
import re


# -- mini tools -- #
def sign(number):
    return cmp(number, 0)


def HHMMSS2DEG(coord_vec):
    return 15 * (float(coord_vec[0]) +
                 float(coord_vec[1]) / 60.0 +
                 float(coord_vec[2]) / 3600.0)


def DDMMSS2DEG(coord_vec):
    return sign(float(coord_vec[0])) * (np.abs(float(coord_vec[0])) +
                                        float(coord_vec[1]) / 60.0 +
                                        float(coord_vec[2]) / 3600.0)


def DEG2HHMMSS(ra_deg):
    ra_hrs = float(ra_deg) / 15.
    hh = int(ra_hrs)
    mm_rem = (ra_hrs - hh) * 60.
    mm = int(mm_rem)
    ss = (mm_rem - mm) * 60.
    return [hh, mm, ss]


def DEG2DDMMSS(dec_deg):
    sgn = sign(dec_deg)
    dec_deg = np.abs(float(dec_deg))
    dd = int(dec_deg)
    mm_rem = (dec_deg - dd) * 60.
    mm = int(mm_rem)
    ss = (mm_rem - mm) * 60.
    return [sgn * dd, mm, ss]
# -- mini tools -- #


# magic constant
G = np.array([[-0.054875539, -0.873437105, -0.483834992],
              [0.494109454, -0.444829594, 0.746982249],
              [-0.867666136, -0.19807639, 0.455983795]]
             )


# The distance in kpc comes from the parallax
# with parallax measured in mas = milli-arcseconds
# D=1/parallax
def parallax2distance(parallax):
    return (1. / parallax)  # kpc


def galacticpropermotion(geo_coord, gal_coord, mu_ra, mu_dec):
    [ra, dec] = geo_coord
    [l, b] = gal_coord

    invG = np.linalg.inv(G)
    H = invG.dot(np.array([-np.sin(l), np.cos(l), 0]))
    H = np.vstack((H,
                   invG.dot(np.array([-np.cos(l) * np.sin(b),
                                      -np.sin(l) * np.sin(b),
                                      np.cos(b)]))
                   ))

    Q = np.array([-np.sin(ra), np.cos(ra), 0])
    Q = np.vstack((Q,
                   np.array([-np.cos(ra) * np.sin(dec),
                             -np.sin(ra) * np.sin(dec),
                             np.cos(dec)])
                   ))
    [mu_l, mu_b] = (H.dot(Q.T)).dot(np.array([[mu_ra], [mu_dec]])).flatten()
    return mu_l, mu_b


def galacticvelocities(galactic_coords, velocities):
    [l, b] = galactic_coords
    [vl, vb, vr] = velocities
    U = vr * np.cos(l) * np.cos(b) - \
        vl * np.sin(l) - \
        vb * np.cos(l) * np.sin(b)
    V = vr * np.sin(l) * np.cos(b) + \
        vl * np.cos(l) - \
        vb * np.sin(l) * np.sin(b)
    W = vr * np.sin(b) + vb * np.cos(b)
    return [U, V, W]


class astrocal(object):
    def __init__(self):
        self.parallax2distance = parallax2distance
        self.galacticpropermotion = galacticpropermotion
        self.galacticvelocities = galacticvelocities

    def HHMMSS2DEG(self, ra_str):
        pattern = re.compile(r'([+-]?\d+)h(\d+)m([\d.]+)s')
        ra = pattern.findall(ra_str)[0]
        return HHMMSS2DEG(np.array(ra, dtype=float))

    def DDMMSS2DEG(self, dec_str):
        pattern = re.compile(r'([+-]?\d+)d(\d+)m([\d.]+)s')
        dec = pattern.findall(dec_str)[0]
        return DDMMSS2DEG(np.array(dec, dtype=float))

    def DEG2HHMMSS(self, ra_deg):
        [hh, mm, ss] = DEG2HHMMSS(ra_deg)
        return "%s:%s:%s" % (str(hh).zfill(2), str(mm).zfill(2), str(ss).zfill(2))

    def DEG2DDMMSS(self, dec_deg):
        [dd, mm, ss] = DEG2DDMMSS(dec_deg)
        return "%s:%s:%s" % (str(dd).zfill(2), str(mm).zfill(2), str(ss).zfill(2))

# -fin-
