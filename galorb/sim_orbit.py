from __future__ import print_function

from astropy import units as u
from galpy.orbit import Orbit
import defs
import galpy
import matplotlib
import numpy as np
import matplotlib.pylab as plt

from .plot_orbit import timeplot, radius, xyz_plot


# galpy orbit simulation
def simulate_orbit(target,
                   integration_period=1.,  # Gyr,
                   integration_method='rk4',
                   mwpotential=None,  # manual MKW model
                   ):
    orbit_radec = Orbit(vxvv=[target['glon'] * u.rad,
                              target['glat'] * u.rad,
                              target['distance'] * u.kpc,
                              target['uvw_src'][0] * (u.km / u.s),
                              target['uvw_src'][1] * (u.km / u.s),
                              target['uvw_src'][2] * (u.km / u.s)],
                        uvw=True,
                        lb=True)

    # velocities are minus the original velocities for backward integration
    orbit_radec.flip(inplace=True)
    ts = np.linspace(0., integration_period, 10000.) * u.Gyr
    # Milky Way potential model
    MWP = galpy.potential.MWPotential2014
    if mwpotential is not None:
        PWP = galpy.potential.PowerSphericalPotentialwCutoff(
            alpha=mwpotential['PWP']['alpha'],
            rc=mwpotential['PWP']['rc'],
            normalize=mwpotential['PWP']['normalize'])
        MNP = galpy.potential.MiyamotoNagaiPotential(
            a=mwpotential['MNP']['a'],
            b=mwpotential['MNP']['b'],
            normalize=mwpotential['MNP']['normalize'])
        NFWP = galpy.potential.NFWPotential(
            a=mwpotential['NFWP']['a'],
            normalize=mwpotential['NFWP']['normalize'])
        MWP = [PWP, MNP, NFWP]

    orbit_radec.integrate(ts, MWP, method=integration_method + '_c')

    return [ts, orbit_radec]


# generate plots
def show_orbit(target_list,
               R0=defs.gal_cen_distance,
               v0=defs.gal_rot_velocity,
               timeseries=False,
               xyz=False,
               toprint=False,
               singles=False):

    num_orbits = len(target_list)
    if num_orbits > 3:
        raise RuntimeError('Cannot display more than 3 overlapped orbits')

    lns = ['-', '--', ':']
    clrs = ['b', 'r', 'k']
    if num_orbits > 1:
        singles = False
        print('Cannot produce individual plots for multiple target config files')
        legend = [target['meta']['name'] for target in target_list]
        figname = 'overplot'
    else:
        legend = None
        figname = ''.join(target_list[0]['meta']['name'].split(' '))

    axis1 = None
    axis2 = None
    if timeseries and not singles:
        fig1, axis1 = plt.subplots(figsize=(15, 7),
                                   ncols=2,
                                   nrows=3,
                                   facecolor='white')
        fig2, axis2 = plt.subplots(figsize=(15, 5),
                                   ncols=2,
                                   nrows=1,
                                   facecolor='white')
    axis3 = None
    if xyz and not singles:
        fig3, axis3 = plt.subplots(figsize=(17, 5),
                                   ncols=3,
                                   nrows=1,
                                   facecolor='white')
        fig3.subplots_adjust(wspace=0.3)

    for idx, target in enumerate(target_list):
        ts = target['ts']
        orbit = target['orbit']
        if toprint:
            ls = lns[idx]
            clr = 'k'
        else:
            ls = '-'
            clr = clrs[idx]

        # allow overlap plots for multiple sources
        if timeseries:
            timeplot(ts,
                     orbit,
                     ls=ls,
                     clr=clr,
                     axis=axis1,
                     singles=singles)
            radius(ts,
                   orbit,
                   ls=ls,
                   clr=clr,
                   axis=axis2,
                   singles=singles)
        if xyz:
            xyz_plot(ts,
                     orbit,
                     r0=R0,
                     v0=v0,
                     axis=axis3,
                     ls=ls,
                     clr=clr,
                     label=target['meta']['name'],
                     singles=singles)

    if timeseries and not singles:
        plt.figure(fig1.number)
        ax = axis1[0, 0]
        if legend is not None:
            ax.legend(legend,
                      bbox_to_anchor=(0, 1),
                      loc='lower left',
                      ncol=len(legend),
                      numpoints=1,
                      prop={'size': 10, 'weight': 'bold'})
        figname1 = figname + '_6params.png'
        print('Integrated orbit image %s' % figname1)
        fig1.savefig(figname1,
                     type="png",
                     orientation='landscape',
                     dpi=300)

        plt.figure(fig2.number)
        ax = axis2[0]
        if legend is not None:
            ax.legend(legend,
                      bbox_to_anchor=(0, 1),
                      loc='lower left',
                      ncol=len(legend),
                      numpoints=1,
                      prop={'size': 10, 'weight': 'bold'})
        figname2 = figname + '_R.png'
        print('Radial components image %s' % figname2)
        fig2.savefig(figname2,
                     type="png",
                     orientation='landscape',
                     dpi=300)

    if xyz and not singles:
        plt.figure(fig3.number)
        ax = axis3[0]
        n_col = 1
        if num_orbits > 1:
            n_col = len(legend)
        ax.legend(bbox_to_anchor=(0, 1),
                  loc='lower left',
                  ncol=n_col,
                  numpoints=1,
                  prop={'size': 10, 'weight': 'bold'})
        figname3 = figname + '_xyz.png'
        print('XYZ orbit components image %s' % figname3)
        fig3.savefig(figname3,
                     type="png",
                     orientation='landscape',
                     dpi=300)

    if singles:
        figures = [manager.canvas.figure
                   for manager in matplotlib._pylab_helpers.Gcf.get_all_fig_managers()]
        for i, fig in enumerate(figures):
            figname = 'figure%d.png' % i
            print('Integrated orbit image %s' % figname)
            fig.savefig(figname,
                        type="png",
                        orientation='landscape',
                        dpi=300)

# -fin-
