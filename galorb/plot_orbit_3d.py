from __future__ import print_function
import matplotlib.pylab as plt


# Display orbits
def plot3d(ts, orbit,
           clr='k',
           ls='-',
           label=None,
           singles=False):
    fig, axis = plt.subplots(figsize=(15, 7),
                             ncols=2,
                             nrows=1,
                             facecolor='white')
    if singles:
        fig, axis = plt.subplots(figsize=(7, 7),
                                 facecolor='white')
        ax = axis
        ax.remove()
        ax = fig.add_subplot(111, projection='3d')
    else:
        ax = axis[0]
        ax.remove()
        ax = fig.add_subplot(121, projection='3d')
    ax.plot(orbit.x(ts),
            orbit.y(ts),
            orbit.z(ts),
            color=clr,
            linestyle=ls)
    ax.scatter(orbit.x(ts)[0],
               orbit.y(ts)[0],
               orbit.z(ts)[0],
               c='k', marker='v',
               label='%s now' % label)
    ax.scatter(orbit.x(ts)[-1],
               orbit.y(ts)[-1],
               orbit.z(ts)[-1],
               c='k', marker='o',
               label='%s 1Gyr ago' % label)
    ax.set_xlabel(r'$\mathbf{x\ (kpc)}$', fontsize=14, fontweight='bold')
    ax.set_ylabel(r'$\mathbf{y\ (kpc)}$', fontsize=14, fontweight='bold')
    ax.set_zlabel(r'$\mathbf{z\ (kpc)}$', fontsize=14, fontweight='bold')
    ax.set_aspect('equal')
    if singles:
        fig, axis = plt.subplots(figsize=(7, 7),
                                 facecolor='white')
        ax = axis
        ax.remove()
        ax = fig.add_subplot(111, projection='3d')
    else:
        ax = axis[1]
        ax.remove()
        ax = fig.add_subplot(122, projection='3d')
    ax.plot(orbit.U(ts),
            orbit.V(ts),
            orbit.W(ts),
            color=clr,
            linestyle=ls)
    ax.scatter(orbit.U(ts)[0],
               orbit.V(ts)[0],
               orbit.W(ts)[0],
               c='k', marker='v',
               label='%s now' % label)
    ax.scatter(orbit.U(ts)[-1],
               orbit.V(ts)[-1],
               orbit.W(ts)[-1],
               c='k', marker='o',
               label='%s 1Gyr ago' % label)
    ax.set_xlabel(r'$\mathbf{U\ (km/s)}$',
                  fontsize=14,
                  fontweight='bold',
                  labelpad=5)
    ax.set_ylabel(r'$\mathbf{V\ (km/s)}$',
                  fontsize=14,
                  fontweight='bold',
                  labelpad=7)
    ax.set_zlabel(r'$\mathbf{W\ (km/s)}$',
                  fontsize=14,
                  fontweight='bold',
                  labelpad=7)
    ax.set_aspect('equal')


# -fin-
