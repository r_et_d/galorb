from __future__ import print_function
import argparse
import os
import sys

from . import core
from ._version import __version__
from .config import readconf, add_targets_from_config, add_target_from_cmd
import defs

try:
    import astropy  # noqa
    cannot_generate_sim = False
except ImportError:
    cannot_generate_sim = True


def main():

    # deal with negative input parameters
    sys.argv = [' ' + a if a[0] == '-' and a[1].isdigit() else a for a in sys.argv]

    usage = '\n{} --astro-file <filename.ini> [orbit simulation options]'.format(
        os.path.basename(sys.argv[0]))
    usage += '\n or'
    usage += '\n{} \
--name <"name">  \
--ra "<ra>"  \
--dec "<dec>"  \
--pm "<pm_ra_cosdec,pm_dec>" \
-p <parallax> [or -d <distance>]  \
--vlsr <vlsr> [or --vr <vr>]  \
[orbit simulation options]'.format(
        os.path.basename(sys.argv[0]))
    description = 'simulate and plot the Galactic orbit of a source'
    parser = argparse.ArgumentParser(
        usage=usage,
        description=description,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--version',
        action='version',
        version=__version__)
    parser.add_argument(
        '--astro-file',
        action='store',
        type=str,
        default=None,
        help='configuration (.ini) file containing target and astrometric information')
    group = parser.add_argument_group(
        title='solar defaults',
        description='position and motion relative the Galactic Center')
    group.add_argument(
        '--R0',
        type=float,
        default=defs.gal_cen_distance,
        help='solar distance to the galactic center [kpc]')
    group.add_argument(
        '--v0',
        type=float,
        default=defs.gal_rot_velocity,
        help='solar circular velocity [km/s]')
    group.add_argument(
        '--uvw_sun',
        nargs=3,
        type=float,
        default=defs.v_bary,
        metavar=('Ubsr', 'Vbsr', 'Wbsr'),
        help='Solar velocity relative to BSR [km/s]')

    group = parser.add_argument_group(
        title='observational input for single target',
        description='target and astrometric velocity measurements')
    group.add_argument(
        '--name',
        type=str,
        help='user defined name of celestial object, ')
    group.add_argument(
        '--ra',
        type=str,
        help='celestial target coordinate, e.g. 00h07m36.24756s')
    group.add_argument(
        '--dec',
        type=str,
        help='celestial target coordinate, e.g. -25d29m40.02813s')
    group.add_argument(
        '-l', '--glon',
        type=float,
        help='galactic longitude [deg]')
    group.add_argument(
        '-b', '--glat',
        type=float,
        help='galactic latitude [deg]')
    group.add_argument(
        '--pm',
        type=str,
        metavar='pm_ra_cosdec,pm_dec',
        help='proper motion vector [mas/yr]')
    dist_ex_group = group.add_mutually_exclusive_group()
    dist_ex_group.add_argument(
        '-p', '--parallax',
        type=float,
        help='trigonometric parallax [mas]')
    dist_ex_group.add_argument(
        '-d', '--distance',
        type=float,
        help='distance to object [kpc]')
    ex_group = group.add_mutually_exclusive_group()
    ex_group.add_argument(
        '--vlsr',
        type=float,
        help='radial velocity with respect to LSR [km/s]')
    ex_group.add_argument(
        '--vr',
        type=float,
        help='radial velocity with respect to BSR [km/s]')
    if not cannot_generate_sim:
        parser.add_argument(
            '--mwp-file',
            action='store',
            type=str,
            default=None,
            help='configuration (.ini) file containing MWPotential model parameters')
        group = parser.add_argument_group(
            title='orbit simulation options')
        group.add_argument(
            '--int-period',
            type=float,
            default=1.,
            help='Integration period [Gyr]')
        choices = ['rk4', 'rk6', 'dopr54', 'leapfrog']
        group.add_argument(
            '--int-method',
            type=str,
            choices=choices,
            default='leapfrog',
            help='Numerical integration method for orbit simulation')
        group.add_argument(
            '--timeseries',
            action='store_true',
            help='Plot overlap of all orbits x,y,z vs time')
        group.add_argument(
            '--xyz',
            action='store_true',
            help='Plot overlap of all cartesian orbits x,y,z')
    group = parser.add_argument_group(
        title='output and display options')
    group.add_argument(
        '-v', '--verbose',
        action='store_true',
        default=False,
        help='Show various graphs and output results')
    group.add_argument(
        '--debug',
        action='store_true',
        default=False,
        help='show more verbose output for debugging')
    if not cannot_generate_sim:
        group.add_argument(
            '--print',
            action='store_true',
            help='Generate images as greyscale for publication')
        group.add_argument(
            '--single',
            action='store_true',
            help='Output graphs as single images.')
    args, _ = parser.parse_known_args()

    targets_dict = {}
    # pack config parameters into default target dictionary
    if args.astro_file is not None:
        add_targets_from_config(args.astro_file, targets_dict)

    # update input parameters with command line arguments if provided
    if args.name is not None:
        cmd_target = args.__dict__
        if args.name not in targets_dict.keys():
            targets_dict[args.name] = dict(defs.target_def)
        add_target_from_cmd(targets_dict,
                            args.name,
                            cmd_target)

    # repackage input targets to list for easy processing
    target_list = []
    for target_name in targets_dict.keys():
        targets_dict[target_name]['name'] = target_name
        target_list.append(targets_dict[target_name])

    # pack MWP config parameters into dict
    orbit_sim_dict = {}
    if not cannot_generate_sim:
        orbit_sim_dict['int_period'] = args.int_period
        orbit_sim_dict['int_method'] = args.int_method
        orbit_sim_dict['mwp'] = None
        if args.mwp_file is not None:
            orbit_sim_dict['mwp'] = readconf(args.mwp_file)
        orbit_sim_dict['plot'] = {'timeseries': args.timeseries,
                                  'xyz': args.xyz,
                                  'print': args.print,
                                  'single': args.single}

    core.main(target_list,
              orbit_sim_dict,
              gal_cen_distance=args.R0,
              gal_rot_velocity=args.v0,
              v_bary=args.uvw_sun,
              verbose=args.verbose,
              debug=args.debug)


if __name__ == '__main__':
    main()

# -fin-
