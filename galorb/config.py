import ConfigParser
import defs


# Read config .ini file
def readconf(filename):
    config_dict = {}

    # config parser returns empty silently if file does not exist
    # manually check file open as dummy to raise error
    with open(filename, 'r'):
        pass

    config = ConfigParser.SafeConfigParser()
    config.read(filename)
    for section in config.sections():
        config_dict[section] = {}
        for option in config.options(section):
            entry = config.get(section, option)
            try:
                config_dict[section][option] = float(entry)
            except:
                config_dict[section][option] = entry

    return config_dict


def add_targets_from_config(
        filename,
        targets_dict={}):
    config_params = readconf(filename)
    for target in config_params.keys():
        targets_dict[target] = dict(defs.target_def)
        for obs_input in config_params[target].keys():
            targets_dict[target][obs_input] = config_params[target][obs_input]
    return targets_dict


def add_target_from_cmd(
        targets_dict,
        new_target_name,
        new_target_dict):
    target = targets_dict[new_target_name]
    for key in new_target_dict.keys():
        ignore_keys = ['name', 'config', 'R0', 'v0', 'uvw_sun']
        if key in ignore_keys:
            pass
        else:
            if new_target_dict[key] is not None:
                target[key] = new_target_dict[key]
    return targets_dict

# -fin-
