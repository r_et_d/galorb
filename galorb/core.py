from __future__ import print_function

import matplotlib.pylab as plt
import numpy as np

from .galactic import build_galactic_target
from .sim_orbit import simulate_orbit, show_orbit


def verify_target_params(target):

    err_msg = 'Information for target {} incomplete:\n\t'.format(
        target['name'])
    # target coordinates
    if target['ra'] is None or target['dec'] is None:
        raise RuntimeError(err_msg + 'target position as RA and DEC coordinates must be provided')
    # for custom calculations the Galactic coordinates must be provided
    # observed motion
    if target['parallax'] is None and target['distance'] is None:
        raise RuntimeError(err_msg + 'observed parallax or distance must be provided')
    if target['pm'] is None:
        raise RuntimeError(err_msg + 'observed proper motion [mas/yr] must be provided')
    else:
        target['pm'] = np.asarray(
            target['pm'].split(','),
            dtype=float)
    if target['vr'] is None:
        if 'vlsr' not in target.keys():
            raise RuntimeError(err_msg + 'radial velocity must be provided')
        if target['vlsr'] is None:
            raise RuntimeError(err_msg + 'radial velocity must be provided')


def show_target_parameters(target,
                           R0=None,
                           v0=None):
        print()
        print('=====Orbital simulation for %s=====' % target['name'])
        print('Celestial coordinates (ra, dec) = (%s, %s)' %
              (target['ra'], target['dec']))
        if target['glon'] is not None:
            print('Galactic coordinates (l, b) = (%.2f, %.2f) [deg]' %
                  (np.rad2deg(target['glon']), np.rad2deg(target['glat'])))
        print('Parallax angle %.2f [mas] = %.2f [kpc]' %
              (target['parallax'], target['distance']))
        print('Observed motion parameters:')
        if 'vlsr' in target.keys():
            print('\tradial velocity relative to LSR %.2f [km/s]' %
                  target['vlsr'])
        print('\theliocentric radial velocity %.2f [km/s]' %
              target['vr'])
        print('\tproper motion (mu_x, mu_y) = (%.2f, %.2f) [mas/yr]' %
              (target['pm'][0], target['pm'][1]))
        print('\tgalactic motion (mu_l, mu_b) = (%.2f, %.2f) [mas/yr]' %
              (target['mul'], target['mub']))
        print('\tgalactic velocities (v_l, v_b) = (%.2f, %.2f) [km/s]' %
              (target['vl'], target['vb']))
        print('Local Standard of Rest')
        print('\tsolar motion (Usun, Vsun, Wsun) = (%.1f, %.1f, %.1f) [km/s]' %
              (target['uvw_sun'][0],
               target['uvw_sun'][1],
               target['uvw_sun'][2]))
        print('\tpeculiar motion (Us, Vs, Ws) = (%.2f, %.2f, %.2f) [km/s]' %
              (target['uvw_src'][0],
               target['uvw_src'][1],
               target['uvw_src'][2]))
        print('Galactocentric')
        if R0 is not None:
            print('\tdistance to galactic center R0 = %.2f [kpc]' % R0)
        if v0 is not None:
            print('\tvelocity at solar circle v0 = %.2f [km/s]' % v0)
        print('\tposition (X, Y, Z) = (%.2f, %.2f, %.2f) [kpc]' %
              (target['xyz'][0],
               target['xyz'][1],
               target['xyz'][2]))
        print('\tspace motion (Vr, Vt, Vz) = (%.2f, %.2f, %.2f) [km/s]' %
              (target['vrtz'][0],
               target['vrtz'][1],
               target['vrtz'][2]))
        print()


def main(
        target_list,
        orbit_sim_dict,
        gal_cen_distance=None,
        gal_rot_velocity=None,
        v_bary=None,
        verbose=False,
        debug=False):

    for idx, target in enumerate(target_list):
        if debug:
            print('\nMotion calculations for target {}'.format(
                target['name']))

        # verify that expected input is available before processing
        verify_target_params(target)
        # calculate galactocentric source motion vectors
        if not build_galactic_target(target,
                                     R0=gal_cen_distance,
                                     v0=gal_rot_velocity,
                                     v_bary=v_bary,
                                     debug=debug):
            # remove target, nothing can be done without correct information
            del target_list[idx]
            continue

        # output results
        if debug or verbose:
            show_target_parameters(target,
                                   R0=gal_cen_distance,
                                   v0=gal_rot_velocity)

        # without orbit simulation parameters, skip the rest
        if not orbit_sim_dict:
            continue

        # simulated orbit
        [ts, orbit] = simulate_orbit(target,
                                     integration_period=orbit_sim_dict['int_period'],
                                     integration_method=orbit_sim_dict['int_method'],
                                     mwpotential=orbit_sim_dict['mwp'])
        target_list[idx] = {'meta': target,
                            'ts': ts,
                            'orbit': orbit}

    if len(target_list) < 1:
        raise RuntimeError('No targets fit simulation criteria, please review input')

    # display simulated orbit
    show_orbit(target_list,
               R0=gal_cen_distance,
               v0=gal_rot_velocity,
               timeseries=orbit_sim_dict['plot']['timeseries'],
               xyz=orbit_sim_dict['plot']['xyz'],
               toprint=orbit_sim_dict['plot']['print'],
               singles=orbit_sim_dict['plot']['single'])

    if verbose:
        try:
            plt.show()
        except:
            pass  # nothing to show


# -fin-
