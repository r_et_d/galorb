"""Python orbit simulation tool using astropy and galorb python science packages"""

# https://packaging.python.org/distributing/ \
#   #standards-compliance-for-interoperability
#
# 1.2.0.dev1  # Development release
# 1.2.0a1     # Alpha Release
# 1.2.0b1     # Beta Release
# 1.2.0rc1    # Release Candidate
# 1.2.0       # Final Release
# 1.2.0.post1 # Post Release
__version__ = '0.0.0.dev0'

__url__ = 'https://bitbucket.org/r_et_d/galorb'

__author__ = 'Ruby van Rooyen'
__email__ = 'ruby.vanrooyen@gmail.com'
