from __future__ import print_function
import numpy as np
import defs
try:
    from astropy import units as u
    import astropy.coordinates as coords
    manual_calc = False
except ImportError:
    import astrocal
    manual_calc = True


def parallax2distance(parallax):
    parallax = float(parallax)  # mas
    if manual_calc:
        distance = astrocal.parallax2distance(parallax)
    else:
        parallax = parallax * u.mas  # mas = milli-arcseconds
        distance = (parallax).to(u.kpc, u.parallax()).value  # kpc = kiloparsecs
    return distance


def celestial2galactic(cel_coords=[],
                       distance=None,
                       gal_coords=[]):
    if manual_calc:
        l = np.radians(gal_coords[0])
        b = np.radians(gal_coords[1])
    else:
        skytarget = coords.SkyCoord(cel_coords[0], cel_coords[1],
                                    distance=distance * u.kpc,
                                    frame='icrs',
                                    unit=(u.hourangle, u.deg))
        galactictarget = skytarget.galactic
        l = galactictarget.l.rad
        b = galactictarget.b.rad
    return l, b


def vlsr2vhsr(vlsr,
              gal_coords,
              v_bary=defs.v_bary):
    # vlsr = vbsr + (Usun*cos(l)*cos(b) + Vsun*sin(l)*cos(b) + Wsun*sin(b)
    # radial velocity
    [Usun, Vsun, Wsun] = np.asarray(v_bary)
    [l, b] = gal_coords
    v_r = vlsr - (Usun * np.cos(l) * np.cos(b) +
                  Vsun * np.sin(l) * np.cos(b) +
                  Wsun * np.sin(b))
    return v_r


def source_peculiar_motion(ra_str,
                           dec_str,
                           distance,
                           pm_ra_cosdec,
                           pm_dec,
                           radial_velocity,
                           l=None,
                           b=None):
    skycoord = None
    if not manual_calc:
        skycoord = coords.ICRS(ra_str, dec_str,
                               distance=distance * u.kpc,
                               pm_ra_cosdec=(pm_ra_cosdec * u.mas / u.yr),
                               pm_dec=(pm_dec * u.mas / u.yr),
                               radial_velocity=radial_velocity * u.km / u.s)
        galacticsky = skycoord.transform_to(coords.Galactic)
        mu_l = galacticsky.pm_l_cosb.value  # mas/yr
        mu_b = galacticsky.pm_b.value  # mas/yr
        [v_l, v_b] = 4.74 * distance * np.asarray((mu_l, mu_b), dtype=float)  # km / s
        galacticsky.representation = 'cartesian'
        Us = galacticsky.U.value
        Vs = galacticsky.V.value
        Ws = galacticsky.W.value
    else:
        ac = astrocal.astrocal()
        ra = np.deg2rad(ac.HHMMSS2DEG(ra_str))
        dec = np.deg2rad(ac.DDMMSS2DEG(dec_str))
        [mu_l, mu_b] = ac.galacticpropermotion(
            (ra, dec),
            (l, b),
            pm_ra_cosdec,
            pm_dec)
        # continue to calculate (Us, Vs, Ws)
        [v_l, v_b] = 4.74 * distance * np.asarray((mu_l, mu_b), dtype=float)  # km / s
        [Us, Vs, Ws] = ac.galacticvelocities(
            (l, b),
            (v_l, v_b, radial_velocity))
    return mu_l, mu_b, v_l, v_b, Us, Vs, Ws, skycoord


def lsr2gsr(src_pec_motion,
            distance,
            l, b,
            R0=defs.gal_cen_distance,
            v0=defs.gal_rot_velocity,
            v_bary=defs.v_bary,
            skycoord=None):
    [Us, Vs, Ws] = np.asarray(src_pec_motion)
    [Usun, Vsun, Wsun] = np.asarray(v_bary)
    if manual_calc:
        [X, Y, Z] = [None, None, None]
        Ugsr = Us + Usun
        Vgsr = Vs + Vsun + v0
        Wgsr = Ws + Wsun
    else:
        v_sun = coords.CartesianDifferential([Usun, Vsun + v0, Wsun] * u.km / u.s)
        Sol = coords.Galactocentric(galcen_distance=R0 * u.kpc,
                                    galcen_v_sun=v_sun)
        skygalacto = skycoord.transform_to(Sol)
        X = skygalacto.x.to(u.kpc)
        Y = skygalacto.y.to(u.kpc)
        Z = skygalacto.z.to(u.kpc)
        Ugsr = skygalacto.v_x.value
        Vgsr = skygalacto.v_y.value
        Wgsr = skygalacto.v_z.value

    Dp = (distance * np.cos(b))  # kpc
    Rp = np.sqrt(R0**2 + Dp**2 - (2. * R0 * Dp * np.cos(l)))
    sinB = Dp / Rp * np.sin(l)
    cosB = (R0 - Dp * np.cos(l)) / Rp

    Vr = Ugsr * cosB - Vgsr * sinB
    Vt = Vgsr * cosB + Ugsr * sinB
    Vz = Wgsr

    return X, Y, Z, Vr, Vt, Vz


# convert helio centric observed parameters to Galactic values relative to LSR
def build_galactic_target(target,
                          R0=defs.gal_cen_distance,
                          v0=defs.gal_rot_velocity,
                          v_bary=defs.v_bary,
                          debug=False):

    if debug:
        print('\tCalculating target galactic coordinates relative to LSR')

    # convert parallax to distance
    if target['distance'] is None:
        target['distance'] = parallax2distance(target['parallax'])
    if debug:
        print('\t distance %.3f kpc (parallax %.3f mas) ' %
              (target['distance'], target['parallax']))

    # convert celestial target coordinates to galactic coordinates
    if manual_calc:
        if target['glon'] is None \
           or target['glat'] is None:
                print('For custom, target galactic (l,b) coordinates must be provided')
                print('Skipping target and continuing....')
                return False
        l, b = celestial2galactic(
            gal_coords=[target['glon'], target['glat']])
    else:
        l, b = celestial2galactic(
            cel_coords=[target['ra'], target['dec']],
            distance=target['distance'])
    target['glon'] = l
    target['glat'] = b
    if debug:
        print('\t coordinates: radec ({}, {}), lb ({:.2f}, {:.2f}) [deg]'.format(
              target['ra'],
              target['dec'],
              np.degrees(target['glon']),
              np.degrees(target['glat'])))

    # convert V_LSR to V_BSR (barycentric)
    target['uvw_sun'] = np.asarray(v_bary)
    [Usun, Vsun, Wsun] = target['uvw_sun']
    if target['vr'] is None:
        target['vr'] = vlsr2vhsr(target['vlsr'],
                                 gal_coords=[target['glon'], target['glat']],
                                 v_bary=v_bary)  # km/s
    if debug:
        print('\t barycentric (Usun={}, Vsun={}, Wsun={}) km/s with radial velocity {:.2f} km/s'.format(
              Usun, Vsun, Wsun, target['vr']))

    # heliocentric motion parameters
    if target['pm'] is None:
        raise RuntimeError('Cannot calculate Galactic motion without observed proper motion')
    pm_ra_cosdec = target['pm'][0]
    pm_dec = target['pm'][1]

    # source peculiar motion relative to LSR
    # calculate (Us, Vs, Ws)
    mu_l, mu_b, v_l, v_b, Us, Vs, Ws, skycoord = source_peculiar_motion(target['ra'],
                                                                        target['dec'],
                                                                        target['distance'],
                                                                        pm_ra_cosdec,
                                                                        pm_dec,
                                                                        target['vr'],
                                                                        l=target['glon'],
                                                                        b=target['glat'])
    target['mul'] = mu_l
    target['mub'] = mu_b
    target['vl'] = v_l
    target['vb'] = v_b
    target['uvw_src'] = np.asarray((Us, Vs, Ws))
    if debug:
        print('\t galactic proper motion (mu_l, mu_b) = ({:.2f}, {:.2f}) mas/yr'.format(
            mu_l, mu_b))
        print('\t galactic velocities (v_l, v_b) = ({:.2f}, {:.2f}) km/s'.format(
            v_l, v_b))
        print('\t source peculiar motion relative to LSR (Us, Vs, Ws) =({:.2f}, {:.2f}, {:.2f}) km/s'.format(
            Us, Vs, Ws))

    # motion relative to Galactic Center of rest
    X, Y, Z, Vr, Vt, Vz = lsr2gsr([Us, Vs, Ws],
                                  target['distance'],
                                  target['glon'],
                                  target['glat'],
                                  R0=R0,
                                  v0=v0,
                                  v_bary=v_bary,
                                  skycoord=skycoord)
    target['vrtz'] = np.array((Vr, Vt, Vz))
    if not manual_calc:
        target['xyz'] = np.array((X.to(u.kpc).value, Y.to(u.kpc).value, Z.to(u.kpc).value))
        if debug:
            print('\t galactrocentric position Cartesian coordinate (X, Y, Z) (%.3f, %.3f, %.3f) [kpc]' %
                  (X.to(u.kpc).value, Y.to(u.kpc).value, Z.to(u.kpc).value))
    if debug:
        print('\t galactrocentric space motion = ({:.2f}, {:.2f}, {:.2f}) km/s'.format(
              Vr, Vt, Vz))

    return target

# -fin-
