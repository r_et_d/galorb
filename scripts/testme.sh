#! /bin/bash

./simOrbit.py --name "SY Scl" --ra 00h07m36.24756s --dec -25d29m40.02813s --parallax 0.75 --pm 5.57,-7.32 --radial-velocity 22 --timeseries --xyz --verbose

./simOrbit.py --config ../config/SYScl.ini --timeseries --xyz --verbose --plot3d

./simOrbit.py --name "Y Lib" --ra 15h11m41.30861s --dec -06d00m41.3727s --parallax 0.855 --uvw -16.00,25.00,13.00 --radial-velocity -7 --timeseries --verbose --xyz --single

./simOrbit.py --config ../config/YLib.ini --timeseries --verbose --xyz --single

make clobber
# -fin-
