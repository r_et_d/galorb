#! /bin/bash

for file in *.png
do
    echo "Converting $file"
    convert $file ${file%.*}.eps
done

# -fin-
