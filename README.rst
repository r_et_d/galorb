galorb
======

GALactic ORBit simulation tool for Python.   
Refer to GALORB [wiki](https://bitbucket.org/r_et_d/galorb/wiki/Home) page for detail instruction

Usage
-----
See [Wiki](https://bitbucket.org/r_et_d/galorb/wiki/Home) page for usage information

- Links and references
  - [SIMBAD Astronomical Database](http://simbad.u-strasbg.fr/simbad/)
  - [astropy](http://docs.astropy.org/en/stable/index.html)
  - [Galactic Dynamics in python](http://iopscience.iop.org/article/10.1088/0067-0049/216/2/29/pdf)
    - https://github.com/jobovy/galpy
    - http://galpy.readthedocs.io/en/latest/index.html


Installation
------------

- Install the project in editable mode with its testing requirements.

    _env/bin/pip install -e ".[testing]"

Requirements
-------------

Compatibility
-------------

Licence
-------

Authors
-------

`galorb` was written by `Ruby van Rooyen <ruby.vanrooyen@gmail.com>`_.
