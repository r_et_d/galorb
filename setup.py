import os
import runpy
import setuptools

_pkg_name = 'galorb'
metadata = runpy.run_path(os.path.join(_pkg_name, '_version.py'))

_path = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(_path, 'README.rst')) as f:
    README = f.read()
# with open(os.path.join(_path, 'CHANGES.txt')) as f:
#     CHANGES = f.read()

requires = [
    'galpy'
    ]

tests_require = [
    'flake8',
    'pytest-cov',
    'tox',
    ]

entry_points = {
    'console_scripts': [
        _pkg_name + '_cli = ' + _pkg_name + '.cli:main',
        ],
    }

# https://packaging.python.org/distributing/
setuptools.setup(
    name=_pkg_name,
    version=metadata['__version__'],

    description=metadata['__doc__'],
    long_description=README,
    url=metadata['__url__'],

    author=metadata['__author__'],
    author_email=metadata['__email__'],

    # license='',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        ],

    # keywords='',

    packages=setuptools.find_packages(
        exclude=[
            '_env',
            'docs',
            'tests',
            ],
        ),
    extras_require={
        # 'development': development_requires,
        'testing': tests_require,
        },
    install_requires=requires,
    scripts=['scripts/simulate_orbit.py'],

    # python_requires='>=3',
    entry_points=entry_points,

    # package_data={
    #     'sample': ['package_data.dat'],
    #     },
    # data_files=[('my_data', ['data/data_file'])],
    )
