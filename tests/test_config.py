import StringIO
import __builtin__

import pytest

from context import config


def test_readconf_fail_with_invalid_filename():
    with pytest.raises(IOError):
        config.readconf('does-not-exist.ini')


def test_readconf_ok_with_empty_file():
    assert {} == config.readconf('/dev/null')


def test_readconf_ok_read(monkeypatch):
    simpleconfig = ("""[section]\nkey = value\n""" +
                    """[section_]\nfloat = 0.0\n\n""")
    buf = StringIO.StringIO(simpleconfig)
    buf.close = lambda: None
    monkeypatch.setattr(__builtin__, 'open', lambda _name, _mode=None: buf)
    assert {
        'section': {'key': 'value'},
        'section_': {'float': 0.0},
        } == config.readconf('dummy')
